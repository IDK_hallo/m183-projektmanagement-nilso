import { getLoggedInUser } from './services/authService.js';
import { initLogin } from './components/login.js';
import { initSongs } from './components/songs.js';

getLoggedInUser()
    .then((user) => {
        if (!user) {
            return initLogin();
        }
        initSongs(user);
    })
    .catch(() => initLogin());
