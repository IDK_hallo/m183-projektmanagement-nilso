import {initRegister} from "./register.js";
import {login as loginUser} from "./../services/authService.js"
import {initSongs} from "./songs.js";

const initLogin = () => {
    document.body.innerHTML = `
<div id="login">
    <div id="loginBox">
        <h1>Login</h1>
             <form class="form" id="form-login">
              <label class="small-font" for="e-mail">Email</label>
              <input
                type="email"
                class="fit-container"
                name="email"
                id="e-mail"
                autocomplete="email"
              />
              <label class="small-font" for="password">Passwort</label>
              <input 
                type="password"
                class="fit-container"
                name="create-password"
                id="password"
                autocomplete="new-password"
              />
              <input
                type="submit"
                class="btn fit-container"
                id="submit"
                value="Login"
              />
              <p id="login-failed" class="failed" style="display: none"></p>
            </form>
               <div>Noch kein Konto? <span id="registerLink" ">Registrieren</span></div>
        </div>
    </div>
</div>
    `;
    document.getElementById("registerLink").addEventListener("click", initRegister)
    document.getElementById("submit").addEventListener("click", (event) => {
        event.preventDefault()
        login(document.getElementById("e-mail").value, document.getElementById("password").value)
    })

    async function login(email, password) {
        const elError = document.getElementById("login-failed");
        if (email && password) {
            await loginUser({ email, password })
                .then((loggedInUser) => {
                    initSongs(loggedInUser);
                })
                .catch(() => {
                    elError.style.display = "block";
                    elError.textContent = 'Login fehlgeschlagen';
                });
        } else {
            elError.style.display = "block";
            elError.textContent = 'Angaben fehlen';
        }
    }
}

export {initLogin};