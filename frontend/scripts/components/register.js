import {initLogin} from "./login.js";
import {register as registerUser} from "./../services/authService.js";


const initRegister = () => {
    document.body.innerHTML = `
<div id="register">
    <div id="registerBox">
        <h1>Registrieren</h1>
            <form class="form" id="form-login">
              <label class="small-font" for="username">Username</label>
              <input
                type="text"
                class="fit-container"
                name="username"
                id="username"
                autocomplete="username"
              />
              <label class="small-font" for="e-mail">Email</label>
              <input
                type="email"
                class="fit-container"
                name="email"
                id="e-mail"
                autocomplete="email"
              />
              <label class="small-font" for="password">Erstelle Passwort</label>
              <input 
                type="password"
                class="fit-container"
                name="createPassword"
                id="create-password"
                autocomplete="new-password"
              />
              
              <label class="small-font" for="password">Bestätige Passwort</label>
              <input 
                type="password"
                class="fit-container"
                name="confirmPassword"
                id="confirm-password"
                autocomplete="new-password"
              />
              
              <label for="genreDropdown">Genre</label>
              <select name="genreDropdown" id="genreDropdown">
                    <option value="">Bitte wählen...</option>
                    <option value="Pop">Pop</option>
                    <option value="Jazz">Jazz</option>
                    <option value="Elektronisch">Elektronisch</option>
                    <option value="Hip Hop">Hip Hop</option>
                    <option value="Funk">Funk</option>
                    <option value="Country">Country</option>
                    <option value="Rock">Rock</option>
                    <option value="Klassisch">Klassisch</option>
              </select>
              <input
                type="submit"
                class="btn fit-container"
                id="submit"
                value="Registrieren"
              />
              <p id="login-failed" class="failed" style="display: none"></p>
            </form>
                <div>Bereits ein Konto? <span id="loginLink" ">Login</span></div>
        </div>
    </div>
</div>`

    document.getElementById("loginLink").addEventListener("click", initLogin)
    document.getElementById("submit").addEventListener("click", (event) => {
        event.preventDefault()
        register(
            document.getElementById("username").value,
            document.getElementById("e-mail").value,
            document.getElementById("create-password").value,
            document.getElementById("confirm-password").value,
            document.getElementById("genreDropdown").value
        );
    });
}

async function register(username, email, createPassword, confirmPassword, genre) {
    const elError = document.getElementById("login-failed");
    if (createPassword !== confirmPassword){
        elError.style.display = "block";
        elError.textContent = 'Passwörter nicht identisch';
    } else {
        if (username && email && confirmPassword && genre) {
            const password = confirmPassword
            await registerUser({username, email, password, genre})
                .then(() => {
                    console.log("hallo")
                    initLogin();
                })
                .catch(() => {
                    elError.style.display = "block";
                    elError.textContent = 'Registrierung fehlgeschlagen';
                });
        } else {
            elError.style.display = "block";
            elError.textContent = 'Angaben fehlen';
        }
    }
}
    export {initRegister}