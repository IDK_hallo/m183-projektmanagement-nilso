import {profileMenu} from "./profileMenu.js";

const initHeader = (loggedInUser) => {
    const elHeader = document.createElement('header');
    elHeader.innerHTML = `
        <div id="username">${loggedInUser.username}</div>
        <div id="profileIcon">
            <img src="./../../assets/img/user.png" alt="profile">
        </div>
    `;
    const getProfileIcon = elHeader.querySelector("#profileIcon")
    getProfileIcon.addEventListener("click", () => profileInfo(loggedInUser))

    return elHeader;
}

function profileInfo(loggedInUser) {
    const getMenu = document.querySelector(".menu");
    if (!getMenu) {
        document.addEventListener("mousedown", () => removeMenu(event));
        const menu = profileMenu(loggedInUser);
        document.body.appendChild(menu);
    } else {
        document.removeEventListener("mousedown", () => removeMenu);
        getMenu.remove()
    }
}

function removeMenu(event) {
    const menu = document.querySelector(".menu");
    if (menu && !menu.contains(event.target)) {
        menu.remove();
    }
}

export {initHeader};
