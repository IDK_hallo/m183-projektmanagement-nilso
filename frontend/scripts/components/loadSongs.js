import { getSongs } from './../services/getSongs.js';
import { createSongBox } from "./createSongBox.js";

const loadSongs = async (user) => {
    try {
        const data = await getSongs(user);
        return data ? createSongForm(data) : null;
    } catch (error) {
        console.error('Error loading songs:', error);
        return null;
    }
};

function createSongForm(data) {
    const elSongs = document.createElement('div');
    elSongs.classList.add('songList');
    elSongs.innerHTML = `
        <div id="userCreatedListBox">
        <h1>Selber herauf geladene Songs</h1></div>
        <div id="userVisibleListBox"><h1>Von anderen Nutzern</h1></div>
    `;

    const getUserCreatedListBox = elSongs.querySelector("#userCreatedListBox");
    data.userCreatedSongs.forEach((song) =>{
        const createBox = createSongBox(song)
        getUserCreatedListBox.appendChild(createBox);
    })
    const getUserVisibleListBox = elSongs.querySelector("#userVisibleListBox");
    data.userVisibleSongs.forEach((song) => {
        const createBox = createSongBox(song)
        getUserVisibleListBox.appendChild(createBox);
    })
    return elSongs;
}

export { loadSongs };
