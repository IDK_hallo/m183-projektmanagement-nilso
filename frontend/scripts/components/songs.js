import {initHeader} from "./header.js";
import {loadSongs} from "./loadSongs.js";

const initSongs = async (loggedInUser) => {
    const header = initHeader(loggedInUser);
    const songsList = await loadSongs(loggedInUser)
    document.body.innerHTML = ``
    document.body.appendChild(header)
    document.body.appendChild(songsList)
}

export {initSongs};
