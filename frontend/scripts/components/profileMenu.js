import {logout as logoutUser} from "../services/authService.js";
import {initLogin} from "./login.js";
import { updateUser } from "./../services/updateUser.js";


const profileMenu = (loggedInUser) => {
    const menuBox = document.createElement('div')
    menuBox.classList.add("menu")
    menuBox.innerHTML = `
        <div class="menuBox">
            <label class="small-font" for="username">Username</label>
                  <input
                    type="text"
                    class="fit-container"
                    name="username"
                    id="usernameInput"
                    autocomplete="username"
                    placeholder=${loggedInUser.username}
                  />
            <button id="saveName">Speichern</button>
            <button id="logout">Logout</button>
        </div>
        `
    const logoutButton = menuBox.querySelector("#logout")
    logoutButton.addEventListener("click", logout)

    const saveButton = menuBox.querySelector("#saveName")
    saveButton.addEventListener("click", () => saveUsername(loggedInUser))

    return menuBox
}

async function saveUsername(loggedInUser) {
    const newName = document.querySelector("#usernameInput").value
    if (newName !== loggedInUser.username) {
        await updateUser({loggedInUser, newName})
            .then(() => {
                loggedInUser.username = newName
                document.querySelector("#username").textContent = newName;
                document.querySelector(".menu").remove()
            })
            .catch((error) => {
                console.log(error)
            });
    }
}

async function logout() {
    await logoutUser().then(() => initLogin());
}


export {profileMenu}