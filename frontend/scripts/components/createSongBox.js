import {saveData} from "../services/saveData.js";

const BASE_URL = '/img'

const createSongBox = (data) => {
    const songBox = document.createElement('div');
    songBox.classList.add("songBox");
    songBox.innerHTML = `
            <form class="songForm">
                <img src="${BASE_URL}/${data.imageName}" alt="">
                <div class="songTitle">${data.title}</div> 
                <div class="inputs">   
                <div class="checkBoxes">
                <input class="songPrivate" type="checkbox" ${data.private ? 'checked' : ''}>Privat</input> <br>
                <input class="songReadonly" type="checkbox" ${data.readonly ? 'checked' : ''}>Nur lesbar</input>
                </div>
                <select name="genreDropdown" class="genreDropdown">
                    <option value="Pop" ${data.visibility === 'Pop' ? 'selected' : ''}>Pop</option>
                    <option value="Jazz" ${data.visibility === 'Jazz' ? 'selected' : ''}>Jazz</option>
                    <option value="Elektronisch" ${data.visibility === 'Elektronisch' ? 'selected' : ''}>Elektronisch</option>
                    <option value="Hip Hop" ${data.visibility === 'Hip Hop' ? 'selected' : ''}>Hip Hop</option>
                    <option value="Funk" ${data.visibility === 'Funk' ? 'selected' : ''}>Funk</option>
                    <option value="Country" ${data.visibility === 'Country' ? 'selected' : ''}>Country</option>
                    <option value="Rock" ${data.visibility === 'Rock' ? 'selected' : ''}>Rock</option>
                    <option value="Klassisch" ${data.visibility === 'Klassisch' ? 'selected' : ''}>Klassisch</option>
                </select> 
                </div>
                <textarea name="songDescription" class="songDescription" cols="30" rows="10">${data.description}</textarea>
                <input type="button" class="submit" value="Speichern">
            </form>
        `;
    const getSubmitButton = songBox.querySelector('.submit')
    getSubmitButton.addEventListener('click', () => saveSong(songBox, data._id))
    return songBox;
};

async function saveSong(songBox, songId){
    const form = songBox.querySelector('.songForm');
    const formData = new FormData(form);

    // Extrahiere Checkbox-Werte und passe sie entsprechend an
    const checkboxValues = {};
    const checkboxes = form.querySelectorAll('input[type="checkbox"]');
    checkboxes.forEach(checkbox => {
        let key = checkbox.className;
        let value = checkbox.checked;
        // Passe die Checkbox-Werte an, wenn sie "private" oder "readonly" sind
        if (key === 'songPrivate') {
            key = 'privat';
        } else if (key === 'songReadonly') {
            key = 'reading';
        }
        checkboxValues[key] = value;
    });

    // Extrahiere Dropdown-Wert
    const dropdownValue = form.querySelector('.genreDropdown').value;

    // Extrahiere Titel-Wert
    const titleValue = form.querySelector('.songTitle').textContent;

    // Extrahiere Textbereich-Wert
    const descriptionValue = form.querySelector('.songDescription').value;

    // Daten zusammenführen
    const data = {
        _id: songId,
        checkboxValues: checkboxValues,
        dropdownValue: dropdownValue,
        titleValue: titleValue,
        descriptionValue: descriptionValue
    };

    // Daten speichern
    await saveData(data);
}



export {createSongBox};
