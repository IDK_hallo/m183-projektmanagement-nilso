const BASE_URL = '/api/songs'

const saveData = (data) => {
    console.log(data)
    axios.put(`${BASE_URL}/update`, data).catch((error) => {
        console.log(error)
    })
}

export {saveData}