const BASE_URL = "/api/songs"

const getSongs = async (user) => {
    try {
        const response = await axios.get(`${BASE_URL}`, user);
        return response.data;
    } catch (error) {
        console.error("Error fetching songs:", error);
        throw error;
    }
};


export {getSongs}