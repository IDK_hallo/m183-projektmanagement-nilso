const BASE_URL = '/user';


const updateUser = (loggedInUser, newName) => {
    const userInfo = {currentUser: loggedInUser, newName:newName}
    const request = axios.put(`${BASE_URL}/updateUser`, userInfo);
    return request.then((response) => response.data);
};

export {updateUser}