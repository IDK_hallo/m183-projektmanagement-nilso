import { Router } from 'express';
import {
    getSongs,
    updateSong
} from './song.controller.js';

const router = Router();

router.get('/', getSongs)
router.put('/update', updateSong)

export { router };
