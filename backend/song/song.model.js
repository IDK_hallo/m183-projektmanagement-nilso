import mongoose from 'mongoose';

const songSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    private: {
        type: Boolean,
        required: true,
    },
    readonly: {
        type: Boolean,
        required: true,
    },
    visibility:{
      type: String,
        enum: ['Alle','Pop','Jazz','Elektronisch','Hip Hop','Funk','Country','Rock','Klassisch'],
        default:'alle',
      required: true,
    },
    description: {
        type: String,
        required: true,
    },
    imageURL:{
        type: String,
        required: true,
    },
    userId:{
        type: String,
        required: true,
    }
});

const model = mongoose.model('songs', songSchema);

export { model };