import {model as Songs} from '../song/song.model.js';


async function getSongs(request, response) {
    const user = request.user
    const userGenre = user.genre
    const userStringId = user._id.toString();
    await Songs.find({visibility: userGenre}).exec().then((data) => {
        const returnList = {
            userCreatedSongs: [],
            userVisibleSongs: []
        }
        data.forEach((song) => {
            if (song.userId.toString() === userStringId) {
                returnList.userCreatedSongs.push(song)
            } else {
                returnList.userVisibleSongs.push(song)
            }
        })
        response.json(returnList)
    }).catch((error) => {
        const status = error.status;
        if (status) {
            response.status(status);
        } else {
            response.status(500);
        }
        response.json({message: error.message});
    });
}

async function updateSong(request, response) {
    try {
        const song = request.body;
        console.log(song);

        // Finde den Song in der Datenbank anhand der ID
        const currentSong = await Songs.findById(song._id).exec();

        // Aktualisiere die Song-Daten mit den Werten aus dem Anfragekörper
        currentSong.title = song.title;
        currentSong.private = song.checkboxValues.privat;
        currentSong.readonly = song.checkboxValues.reading;
        currentSong.visibility = song.dropdownValue;
        currentSong.description = song.descriptionValue;

        await currentSong.save();

        response.status(200).json({ message: 'Song erfolgreich aktualisiert', song: currentSong });
    } catch (error) {
        const status = error.status || 500;
        response.status(status).json({ message: error.message });
    }
}


export {getSongs, updateSong}