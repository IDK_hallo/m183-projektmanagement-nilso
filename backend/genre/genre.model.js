import mongoose from 'mongoose';

const genresSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    admins: [
        {
            type: mongoose.ObjectId,
            ref: 'users',
            required: true,
        },
    ],
    members: [
        {
            type: mongoose.ObjectId,
            ref: 'users',
            required: true,
        },
    ],
    songs: [
        {
            type: mongoose.ObjectId,
            ref: 'songs',
            required: true,
        },
    ],
});

const model = mongoose.model('genres', genresSchema);

export { model };