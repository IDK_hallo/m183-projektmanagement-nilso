import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    username: {
        type:String,
        required:true,
    },
    email: {
        type: String,
        required: true,
    },
    genre: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
});

const model = mongoose.model('users', userSchema);

export { model };