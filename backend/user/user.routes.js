import { Router } from 'express';
import {
    updateUser,
} from './user.controller.js';
import { isAuthenticated } from './../auth/authorization.js';

const router = Router();

router.put('/updateUser',isAuthenticated, updateUser);

export { router };