import {model as User} from '../user/user.model.js';

async function updateUser(request, response) {
    const loggedInUser = request.body.currentUser.loggedInUser;
    const newName = request.body.currentUser.newName
    await User.findOne({ username: newName }).then(async (user) => {
        if (!user) {
            const currentToUpdateUser = await User.findOne(loggedInUser).exec();
            currentToUpdateUser.username = newName;
            await currentToUpdateUser.save()
                .then((savedUser) => {
                    response.json(savedUser);
                })
                .catch((error) => {
                    if (error.status) {
                        response.status(error.status);
                    } else {
                        response.status(500);
                    }
                    response.json({ message: error.message });
                });
        } else {
            return Promise.reject({
                message: `User with username ${newName} already exists.`,
                status: 409,
            });
        }
    }).catch((error) => {
        console.error("Error:", error);
    });

}


export {updateUser}