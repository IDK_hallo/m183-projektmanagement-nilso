import { model as Genre } from './../genre/genre.model.js';
import { model as Song } from './../song/song.model.js';

function isAuthenticated(request, response, next) {
    if (!request.isAuthenticated()) {
        return response.status(401).json({ message: 'Not authenticated' });
    }
    next();
}

async function isAllowedToChangeTeam(request, response, next) {
    const userId = request.user._id;
    const teamId = request.params.id;
    const foundGenre = await Genre.findById(teamId);
    if (!foundGenre) {
        return response
            .status(400)
            .json({ message: `Team with id ${teamId} does not exist.` });
    }
    if (!isUserTeamAdmin(userId, foundGenre)) {
        return response.status(401).json({ message: 'Not allowed to change genre' });
    }
    request.team = foundGenre;
    next();
}

async function isAllowedToChangeTodo(request, response, next) {
    const user = request.user._id;
    const songsId = request.params.id;
    const [foundSong, foundGenre] = await Promise.all([
        Song.findById(todoId).exec(),
        Genre.findOne({ songs: songsId }).exec(),
    ]);
    if (!foundSong) {
        return response
            .status(400)
            .json({ message: `Todo with id ${todoId} does not exist.` });
    }
    if (!foundGenre) {
        return response.status(400).json({
            message: `Team that contains todo with id ${todoId} does not exist.`,
        });
    }
    if (!isUserAllowedToChangeTodo(user._id, foundSong.responsible, foundGenre)) {
        return response.status(401).json({ message: 'Not allowed to change todo' });
    }
    request.todo = foundSong;
    request.team = foundGenre;
    next();
}

function isUserAllowedToChangeTodo(userId, responsibleId, team) {
    if (userId.equals(responsibleId)) {
        return true;
    }
    return isUserTeamAdmin(userId, team);
}

function isUserTeamAdmin(userId, team) {
    const foundAdmin = team.admins.find((adminId) => adminId.equals(userId));
    return !!foundAdmin;
}

export { isAuthenticated, isAllowedToChangeTeam, isAllowedToChangeTodo };