import { model as User } from '../user/user.model.js';
import { hash } from 'bcrypt';
import passport from 'passport';

async function registerUser(request, response) {
    const email = request.body.email;
    await User.findOne({ email: email })
        .exec()
        .then(async (user) => {
            if (user) {
                return Promise.reject({
                    message: `User with email ${email} already exists.`,
                    status: 409,
                });
            }
            const hashedPwd = await hash(request.body.password, 10);
            return User.create({
                username: request.body.username,
                email: email,
                genre: request.body.genre,
                password: hashedPwd,
            }).then((user) => {
                response.json({
                    _id: user._id,
                    username: user.username,
                    email: user.email,
                    genre: user.genre,
                });
            });
        })
        .catch((error) => {
            const status = error.status;
            if (status) {
                response.status(status);
            } else {
                response.status(500);
            }
            response.json({ message: error.message });
        });
}

function getUser(request, response) {
    const user = request.user;
    if (!user) {
        return response.json();
    }
    response.json({
        _id: user._id,
        username: user.username,
        email: user.email,
        genre: user.genre,
    });
}

function loginUser(request, response, next) {
    passport.authenticate('local', function (error, user, info) {
        if (error) {
            return next(error);
        }
        if (!user) {
            console.log(info);
            return response.status(401).json({ message: 'Authentication failed' });
        }
        request.logIn(user, () => {
            response.json({
                _id: user._id,
                username: user.username,
                email: user.email,
                genre: user.genre,
            });
        });
    })(request, response, next);
}

function logoutUser(request, response, next) {
    request.logOut((error) => {
        if (error) {
            console.log('Failed to logout');
            return next(error);
        }
        request.session.destroy((error) => {
            if (error) {
                console.log('Failed to destroy session');
                return next(error);
            }
            response.clearCookie('connect.sid');
            response.json({ message: 'Successfully logged out' });
        });
    });
}

export { registerUser, getUser, loginUser, logoutUser };