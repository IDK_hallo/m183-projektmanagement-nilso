import passport from 'passport';
import LocalStrategy from 'passport-local';
import expressSession from 'express-session';
import { compare } from 'bcrypt';
import mongoose from 'mongoose';
import MongoStore from 'connect-mongo';
import { model as User } from '../user/user.model.js';

function init(app) {
    async function verify(email, password, done) {
        try {
            const foundUser = await User.findOne({ email: email }).exec();
            if (!foundUser) {
                return done(null, false, { message: `No user with email ${email}` });
            }
            if (await compare(password, foundUser.password)) {
                return done(null, foundUser);
            } else {
                return done(null, false, {
                    message: `Password of user with email ${email} incorrect`,
                });
            }
        } catch (error) {
            return done(error);
        }
    }
    passport.use(new LocalStrategy({ usernameField: 'email' }, verify));
    passport.serializeUser((user, done) => done(null, user._id));
    passport.deserializeUser(async (id, done) => {
        try {
            const foundUser = await User.findById(id).exec();
            if (!foundUser) {
                done('User not found');
            }
            return done(null, foundUser);
        } catch (error) {
            return done(error);
        }
    });
    app.use(
        expressSession({
            secret: process.env.SESSION_SECRET,
            resave: false,
            saveUninitialized: false,
            store: MongoStore.create({
                client: mongoose.connection.getClient(),
            }),
            cookie: {
                path: '/',
                httpOnly: true,
                secure: false,
                maxAge: 1000 * 60 * 60, // one hour
            },
        })
    );
    app.use(passport.initialize());
    app.use(passport.session());
}

export { init };